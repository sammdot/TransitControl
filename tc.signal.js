/*
 * Signal module for the Transit Control display.
 * (c) Sammi de Guzman, 2016.
 */

Signal = function(station, direction, signal) {
	this.station = station;
	this.direction = direction;
	this.time = 0;
	this.timerActive = false;
	this.update(signal);

	Signal.upperHeadColors = ["red", "green", "amber", "amber"];
	Signal.lowerHeadColors = ["red", "green", "amber", "green"];

	return this;
};

Signal.prototype.update = function (signal) {
	this.aspectType = signal.aspectype;
	this.aspect = signal.aspect;
	this.lastTime = signal.lastime;
	if (this.aspect != 0) {
		this.time = 0;
		this.timerActive = false;
	} else if (signal.time && this.canBeTimed()) {
		if (!this.timerActive) {
			this.time = signal.time;
		}
	} else {
		this.time = 0;
	}
};

Signal.prototype.aspectColors = function () {
	if (this.aspectType == 0) {
		return [Signal.upperHeadColors[this.aspect]];
	} else {
		return [Signal.upperHeadColors[this.aspect], Signal.lowerHeadColors[this.aspect]];
	}
};

Signal.prototype.isFavorable = function () {
	return this.aspect != 0;
};

Signal.prototype.isInterlocking = function () {
	return this.aspectType != 0;
};

Signal.prototype.isDiverging = function () {
	return this.aspectType == 4 && this.aspect == 2;
};

Signal.prototype.canBeTimed = function () {
	return this.aspect == 0 && this.aspectType != 0;
};

Signal.prototype.isTimed = function () {
	return this.canBeTimed() && this.time && this.time != 0;
};
