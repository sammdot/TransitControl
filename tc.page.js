var interval, timeUntilRefresh, tc, completion;

hideError = function () { document.getElementById("error").classList.remove("error"); };
showError = function () { document.getElementById("error").classList.add("error"); };

reloadData = function () {
	tc.reloadData(function () {
		hideError();
		completion.list = Object.keys(tc.names);
		document.querySelector("[data-action='move-original']").innerHTML = "&raquo; towards " + tc.names[tc.termini[0]];
		document.querySelector("[data-action='move-reverse']").innerHTML = "&raquo; towards " + tc.names[tc.termini[1]];
	}, showError);
};

onSecondLapse = function () {
	var dots = ".".repeat(timeUntilRefresh - 1);
	document.getElementById("time-until-refresh").textContent = dots;
	if (tc.layout) {
		tc.updateTimes();
		tc.drawStationLastTimes();
		tc.drawTrainLastTimes();
		tc.drawSignalTimes(true);
	}
	timeUntilRefresh--;
	if (timeUntilRefresh == 0) {
		timeUntilRefresh = interval;
		reloadData();
	}
};

document.body.onload = function () {
	hideError();

	// Initialize the clock on the bottom-right
	time_is_widget.init({Ontario_z18a:{}});

	// Initialize Transit Control
	tc = new TransitControl("display", "trains", "train-menu", "clock-menu", {
		font: "monoOne",
		layoutColor: "#666",
		trainColor: "white",
		redAlarmColor: "#e04040",
		yellowAlarmColor: "#e0e000",
		trainMinorIssueColor: "#e08000",
		runNumberColor: "white",
		stationNameSize: 13,
		runNumberSize: 14,
		signalSelectorLabelSize: 10,
		stationLastTimeSize: 11,
		aspectColors: {
			red:   "#cc0000",
			amber: "#ffa500",
			green: "#228822"
		},
		fadedAspectColors: {
			red:   "rgba(204,0,0,0.5)",
			amber: "rgba(255,165,0,0.5)",
			green: "rgba(0,102,0,0.5)"
		}
	})
	
	tc.resizeCanvas();
	tc.updateTimes();

	interval = 3;

	// Initialize station code completion
	completion = new Awesomplete(document.getElementById("station-code"), {
		minChars: 0,
		item: function (text, input) {
			var el = document.createElement("li");
			el.textContent = text;
			var span = document.createElement("span");
			span.classList.add("station-name");
			span.textContent = tc.names[text];
			el.appendChild(span);
			return el;
		},
		sort: function (first, second) {
			function compareLetters (x, y) { return x === y ? 0 : (x > y ? 1 : -1); }
			var a = first.value,
				b = second.value;
			return compareLetters(a[0], b[0]) || compareLetters(a[1], b[1])
				|| compareLetters(a[2], b[2]) || a.length - b.length
				|| compareLetters(a[3], b[3]);
		},
		filter: function (text, input) { return text.indexOf(input.toUpperCase()) == 0; },
		list: []
	});
	completion.input.onfocus = function () {
		if (completion.input.value === "") {
			completion.open();
			completion.evaluate();
		}
	};

	// Begin the update cycle
	timeUntilRefresh = interval;
	onSecondLapse();
	reloadData();
	setInterval(onSecondLapse, 1000);
};

window.onresize = function () {
	tc.resizeCanvas();
};

document.getElementById("log-off").onclick = function () {
	tc.logoff(badge);
};
