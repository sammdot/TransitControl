/*
 * Canvas section of the Transit Control display.
 * (c) Sammi de Guzman, 2016.
 */

TransitControl.prototype.resizeCanvas = function () {
	this.canvas.height = this.canvas.element.parentElement.clientHeight;
	this.canvas.width = this.canvas.element.parentElement.clientWidth;

	var c = this.canvas.context;
	c.canvas.height = this.canvas.height;
	c.canvas.width = this.canvas.width;

	if (this.layout) {
		this.drawCanvas();
	}
};

TransitControl.prototype.drawCanvas = function () {
	var c = this.canvas.context;
	c.clearRect(0, 0, this.canvas.width, this.canvas.height);

	this.layout.stationCenters.forEach(function (stnCenter, index) {
		this.drawPlatforms(this.stations[index], stnCenter, this.layout.platforms[index]);
	}, this);

	this.drawStationLastTimes();
	this.drawTrainLastTimes();
	this.drawSignalTimes(false);

	this.layout.tracks.forEach(function (track) { this.drawTrack(track); }, this);
	for (run in this.trains) { this.drawTrain(this.trains[run]); };

	Object.keys(this.signals).forEach(function (stn) {
		this.signals[stn].forEach(function (sig) {
			sig && this.drawSignal(sig);
		}, this);
	}, this);
};

TransitControl.prototype.drawPlatforms = function (stnName, stnCenter, platformType) {
	var c = this.canvas.context;
	c.fillStyle = this.style.layoutColor;
	c.strokeStyle = "";

	var x = stnCenter[0], y = stnCenter[1],
		height = 5, width = 24;

	if (platformType == 0) { // center platform
		c.fillRect(x - (width / 2), y - height, width, height * 2);
	} else if (platformType == 1) { // side platform
		c.fillRect(x - (width / 2), y - 16 - (height / 2), width, height);
		c.fillRect(x - (width / 2), y + 16 - (height / 2), width, height);
	} else if (platformType == 2) { // Union
		c.fillRect(x - (width / 2), y - 16 - (height / 2), width, height);
		c.fillRect(x - (width / 2), y, width, height);
	} else if (platformType == 3) { // Davisville
		c.fillRect(x - (width / 2), y - 16 - (height / 2), width, height);
		c.fillRect(x - (width / 2), y + 16 - (height / 2), width, height * 2);
	} else if (platformType == 4) { // Wilson
		c.fillRect(x - (width / 2), y - height, width, height * 2);
		c.fillRect(x - (width / 2) + 50, y - 34 - (height / 2), width, height);
	} else if (platformType == 5) { // Sheppard-Yonge
		c.fillRect(x - (width / 2), y - 16 - (height / 2), width, height);
		c.fillRect(x - (width / 2), y - height, width, height * 2);
		c.fillRect(x - (width / 2), y + 16 - (height / 2), width, height);
	}

	c.font = this.style.stationNameSize + "px " + this.style.font;
	var textWidth = c.measureText(stnName).width;
	c.fillText(stnName, x - (textWidth / 2), y - 25);
	c.fillStyle = "";
};

TransitControl.prototype.drawStationLastTime = function (stnCenter, index) {
	var c = this.canvas.context,
		x = stnCenter[0], y = stnCenter[1];

	function formatLastTime (time) {
		var secs = time % 60,
			mins = Math.floor(time / 60),
			hrs;
		if (mins > 59) {
			hrs = Math.floor(mins / 60);
			mins = mins % 60;
		}

		return (hrs ? hrs + ":" + (mins < 10 ? "0" : "") : "") + 
			mins + ":" + (secs < 10 ? "0" : "") + secs;
	};

	var self = this;
	function lastTimeToColor (time) {
		if (self.line == 1 || self.line == 4) {
			return time <= 240 ? "green" : (time <= 360 ? "amber" : "red");
		} else if (self.line == 2) {
			return time <= 300 ? "green" : (time <= 480 ? "amber" : "red");
		} else {
			return "red";
		}
	};

	var signalId = this.layout.signalsToStations[index];
	if (!this.signals[signalId]) return;
	var now = Math.floor(new Date().getTime() / 1000),
		lastTime0 = now - this.signals[signalId][0].lastTime,
		lastTime1 =
			this.line == 2 ? null : // TODO: BD
			now - this.signals[signalId][1].lastTime,
		lastTimeText0 = lastTime0 ? formatLastTime(lastTime0) : "",
		lastTimeText1 = lastTime1 ? formatLastTime(lastTime1) : "";

	c.font = this.style.stationLastTimeSize + "px " + this.style.font;

	if (lastTime0 < 24 * 60 * 60) {
		var size = c.measureText(lastTimeText0);
		c.fillStyle = this.style.aspectColors[lastTimeToColor(lastTime0)];
		c.clearRect(x - (size.width / 2), y + 30, size.width, 12);
		c.fillText(lastTimeText0, x - (size.width / 2), y + 40);
	}
	if (lastTime1 < 24 * 60 * 60) {
		var size = c.measureText(lastTimeText1);
		c.fillStyle = this.style.aspectColors[lastTimeToColor(lastTime1)];
		c.clearRect(x - (size.width / 2), y - 50, size.width, 12);
		c.fillText(lastTimeText1, x - (size.width / 2), y - 40);
	}
	c.fillStyle = "";
};

TransitControl.prototype.drawStationLastTimes = function () {
	this.layout.stationCenters.forEach(function (stnCenter, index) {
		this.drawStationLastTime(stnCenter, index);
	}, this);
};

TransitControl.prototype.drawTrainLastTime = function (train) {
	var c = this.canvas.context,
		index = train.location.station,
		locations = this.layout.trainLocations[train.location.direction][index];

	if (!locations) return;

	var position = locations[train.location.enroutesPast][train.location.departed],
		x = position[0], y = position[1];

	function formatLastTime (time) {
		var secs = time % 60,
			mins = Math.floor(time / 60),
			hrs;
		if (mins > 59) {
			hrs = Math.floor(mins / 60);
			mins = mins % 60;
		}

		return (hrs ? hrs + ":" + (mins < 10 ? "0" : "") : "") + 
			mins + ":" + (secs < 10 ? "0" : "") + secs;
	};

	var now = Math.floor(new Date().getTime() / 1000),
		lastTime = now - train.lastTime,
		lastTimeText = formatLastTime(lastTime);

	c.font = this.style.stationLastTimeSize + "px " + this.style.font;

	var size = c.measureText(lastTimeText);
	c.fillStyle = "white";
	c.clearRect(x - (size.width / 2),
		train.location.direction == 0 ? y + 21 : y - 41, size.width, 12);
	c.fillText(lastTimeText, x - (size.width / 2),
		train.location.direction == 0 ? y + 31 : y - 31);
	c.fillStyle = "";
};

TransitControl.prototype.drawTrainLastTimes = function () {
	Object.keys(this.trains).forEach(function (run) {
		this.drawTrainLastTime(this.trains[run]);
	}, this);
};

TransitControl.prototype.drawTrack = function (track) {
	var c = this.canvas.context;
	c.fillStyle = "";
	c.strokeStyle = this.style.layoutColor;

	c.beginPath();
	c.moveTo(track[0][0], track[0][1]);
	for (var i = 1; i < track.length; ++i) {
		c.lineTo(track[i][0], track[i][1]);
	}
	c.stroke();
	c.strokeStyle = "";
};

TransitControl.prototype.drawTrain = function (train) {
	var c = this.canvas.context;
	c.fillStyle = "";
	c.strokeStyle = "";

	var loc = train.location;
	var storageTrack = !Number.isInteger(loc.station);
	var position;
	if (storageTrack) {
		position = this.layout.storageTrackLocations[loc.station];
	} else {
		position = this.layout.trainLocations[loc.direction][loc.station][loc.enroutesPast][loc.departed];
	}

	var x = position[0], y = position[1],
		height = 4, width = 24;

	train.screenPosition = position;
	
	if (train.redAlarm) c.fillStyle = this.style.redAlarmColor;
	else if (train.yellowAlarm) c.fillStyle = this.style.yellowAlarmColor;
	else if (train.hasMinorIssue()) c.fillStyle = this.style.trainMinorIssueColor;
	else c.fillStyle = this.style.trainColor;
	c.fillStyle = c.fillStyle || this.style.trainColor;
	c.fillRect(x - (width / 2), y - (height / 2), width, height);

	c.font = this.style.runNumberSize + "px " + this.style.font;

	c.fillStyle = "";
	if (train.redAlarm) c.fillStyle = this.style.redAlarmColor;
	else if (train.yellowAlarm) c.fillStyle = this.style.yellowAlarmColor;
	else if (train.hasMinorIssue()) c.fillStyle = this.style.trainMinorIssueColor;
	else c.fillStyle = this.style.runNumberColor;
	c.fillStyle = c.fillStyle || this.style.runNumberColor;

	if (storageTrack) {
		c.fillText(train.run, x - 40, y + 4.5);
	} else {
		c.fillText(train.run, x - (width / 2), y + (loc.direction == 0 ? 16 : -6));
	}
	c.fillStyle = "";

	if (train.redAlarm || train.yellowAlarm) {
		if (train.redAlarm) c.fillStyle = this.style.redAlarmColor;
		else if (train.yellowAlarm) c.fillStyle = this.style.yellowAlarmColor;
		else c.fillStyle = this.style.runNumberColor;
		c.fillStyle = c.fillStyle || this.style.runNumberColor;
		c.beginPath();
		c.arc(x - (width / 2) - 14, y, 8, 0, Math.PI * 2);
		c.fill();
		c.fillStyle = train.yellowAlarm ? "#222" : "white";
		c.font = "bold " + this.style.runNumberSize + "px " + this.style.font;
		c.fillText("!", x - (width / 2) - 18.5, y + 5);
		c.fillStyle = "";
		c.font = "";
	}
};

TransitControl.prototype.drawSignal = function (signal) {
	var c = this.canvas.context,
		radius = 3.5;

	if (!this.layout.signals[signal.station]) { return; }
	var position = this.layout.signals[signal.station].signals[signal.direction],
		aspects = signal.aspectColors();

	if (!position) { return; }

	c.strokeStyle = "";
	c.beginPath();
	c.fillStyle = this.style.aspectColors[aspects[signal.isInterlocking() ? 1 : 0]];
	c.arc(position[0] + (signal.direction == 0 ? radius : -radius), position[1],
		radius, 0, Math.PI * 2, true);
	c.fill();
	if (signal.isInterlocking()) {
		c.beginPath();
		c.fillStyle = this.style.aspectColors[aspects[0]];
		c.arc(position[0] + (signal.direction == 0 ? 1 : -1) * (3 * radius + 1),
			position[1], radius, 0, Math.PI * 2, true);
		c.fill();
	}
	c.fillStyle = "";

	var selectors = this.layout.signals[signal.station].selectors,
		selector = selectors ? selectors[signal.direction] : null;
	if (selector) {
		this.drawSignalSelector(signal,
			this.layout.signals[signal.station].label,
			signal.direction);
	}
};

TransitControl.prototype.drawSignalTime = function (signal, label, direction, timerRunning) {
	var c = this.canvas.context,
		position = this.layout.signals[signal.station].selectors[direction];

	var timeLeft = signal.time,
		timeLeftText = timeLeft <= 0 ? "--" : "" + timeLeft;
	c.fillStyle = "white";
	c.font = this.style.signalSelectorLabelSize + "px " + this.style.font;
	c.clearRect(position[0] - c.measureText("  ").width -
		(signal.timerActive ? 8 : 18),
		position[1] + this.style.signalSelectorLabelSize * 0.8 - 6,
		c.measureText("  ").width, 8);
	c.fillText(timeLeftText, position[0] - c.measureText(timeLeftText).width -
		(signal.timerActive ? 8 : 18),
		position[1] + this.style.signalSelectorLabelSize * 0.8 + 1);
	c.fillStyle = "";
	c.font = "";

	if (signal.timerActive && timerRunning) {
		signal.time--;
		if (signal.time == 0) {
			this.cancelSignalTime(signal.station, signal.direction);
			this.changeSignal(signal.station, signal.direction, 1);
			signal.aspect = 1;
			signal.timerActive = false;
		}
	}
};

TransitControl.prototype.drawSignalTimes = function (timerRunning) {
	Object.keys(this.signals).forEach(function (stn) {
		this.signals[stn].forEach(function (signal) {
			if (signal) {
				var c = this.canvas.context,
					position = this.layout.signals[signal.station] &&
						this.layout.signals[signal.station].selectors &&
						this.layout.signals[signal.station].selectors[signal.direction];
				if (position && signal.canBeTimed() && !signal.isTimed()) {
					c.clearRect(position[0] - c.measureText("  ").width - 18,
						position[1] + this.style.signalSelectorLabelSize * 0.8 - 6,
						c.measureText("  ").width, 8);
				}

				if (signal.isTimed()) {
					var self = this;
					function enrouteOccupied() {
						stn = signal.station;
						stn_ = -1;
						while (true) {
							stn_ = self.layout.signalsToStations.indexOf(stn);
							if (stn_ >= 0) { break; }
							else { stn--; }
						}
						if (self.layout.signalsToStations[stn_] == stn) { return; }
						return self.layout.enrouteToSignal[stn_].indexOf(stn) > -1;
					}
					if (this.occupiedStations[signal.direction]
							.indexOf(this.layout.signalsToStations.indexOf(signal.station)) > -1 ||
						enrouteOccupied()) {
						signal.timerActive = true;
					}
					this.drawSignalTime(signal, this.layout.signals[signal.station].label, signal.direction, timerRunning);
				}
			}
		}, this);
	}, this);
};

TransitControl.prototype.drawSignalSelector = function (signal, label, direction) {
	var c = this.canvas.context,
		radius = 3.5;

	var position = this.layout.signals[signal.station].selectors[direction];

	signal.aspectBox = {};

	c.fillStyle = this.style.layoutColor;
	c.strokeStyle = "";
	c.font = this.style.signalSelectorLabelSize + "px " + this.style.font;
	if (label.indexOf(".") != -1) {
		var parts = label.split(/\./),
			sizes = [c.measureText(parts[0]).width, c.measureText(parts[1]).width];
		if (signal.canBeTimed()) {
			c.fillText(parts[1], position[0] - sizes[1] - 8, position[1] - 1);
		} else {
			c.fillText(parts[0], position[0] - sizes[0] - 8, position[1] - 1);
			c.fillText(parts[1], position[0] - sizes[1] - 8, position[1] +
				this.style.signalSelectorLabelSize * 0.8 + 1);
		}
	} else {
		c.fillText(label, position[0] - c.measureText(label).width - 8,
			signal.canBeTimed() ? position[1] - 1 :
				position[1] + this.style.signalSelectorLabelSize / 3);
	}

	if (signal.canBeTimed() && !signal.timerActive) {
		var clockRadius = 4,
			clockCenter = [position[0] - clockRadius - 8, position[1] + 5];
		c.fillStyle = "white";
		c.beginPath();
		c.arc(clockCenter[0], clockCenter[1], clockRadius, 0, Math.PI * 2);
		c.fill();
		c.fillStyle = "";
		c.strokeStyle = "#444";
		c.beginPath();
		c.moveTo(clockCenter[0], clockCenter[1] - clockRadius + 1);
		c.lineTo(clockCenter[0], clockCenter[1]);
		c.lineTo(clockCenter[0] + clockRadius - 1, clockCenter[1]);
		c.stroke();
		c.strokeStyle = "";
		signal.clockSelector = [clockCenter[0], clockCenter[1], clockRadius];
	} else {
		delete signal.clockSelector;
	}

	var aspects = signal.aspectType == 4 ? [0, 1, 2, 3] : [0, 1, 3];

	aspects.forEach(function (asp, index) {
		var pos = [position[0] + 3 * index * radius, position[1]],
			aspectColors = signal.aspect == asp ? this.style.aspectColors :
				this.style.fadedAspectColors;
		c.fillStyle = aspectColors[Signal.upperHeadColors[asp]];
		c.beginPath();
		c.arc(pos[0], pos[1] - radius - 0.5, radius, 0, Math.PI * 2);
		c.fill();
		c.fillStyle = aspectColors[Signal.lowerHeadColors[asp]];
		c.beginPath();
		c.arc(pos[0], pos[1] + radius + 0.5, radius, 0, Math.PI * 2);
		c.fill();
		signal.aspectBox[asp] = [pos[0] - radius * 1.5, pos[1] - 2 * radius - 2,
			radius * 3, 4 * radius + 4];
	}, this);

	c.fillStyle = "";
	c.strokeStyle = "white";
	c.beginPath();
	var box = signal.aspectBox[signal.aspect];
	c.strokeRect.apply(c, box);
	c.strokeStyle = "";
};

TransitControl.prototype.canvasClicked = function (e) {
	var x = (e.pageX || (e.clientX + document.body.scrollLeft +
			document.documentElement.scrolLeft)) - this.canvas.element.offsetLeft,
		y = (e.pageY || (e.clientY + document.body.scrollTop +
			document.documentElement.scrollTop)) - this.canvas.element.offsetTop;

	var clickedTrain;
	Object.keys(this.trains).forEach(function (run) {
		var pos = this.trains[run].screenPosition;
		if (Math.abs(pos[0] - x) <= 16 && Math.abs(pos[1] - y) <= 8) {
			clickedTrain = this.trains[run];
		}
	}, this);

	if (clickedTrain) {
		this.toggleStrip(this.strips[clickedTrain.run]);
		return;
	}

	var clickedSignal, desiredAspect, clickedClock;
	Object.keys(this.signals).forEach(function (stn) {
		Object.keys(this.signals[stn]).forEach(function (dir) {
			if (!this.signals[stn][dir]) { return; }
			var aspects = this.signals[stn][dir].aspectBox;
			if (!aspects) { return; }
			Object.keys(aspects).forEach(function (aspect) {
				var box = aspects[aspect];
				var dx = x - box[0], dy = y - box[1];
				if (dx >= 0 && dx <= box[2] &&
					dy >= 0 && dy <= box[3]) {
					clickedSignal = this.signals[stn][dir];
					desiredAspect = aspect;
				}
			}, this);

			var clock = this.signals[stn][dir].clockSelector;
			if (!clock) { return; }
			var dx = x - clock[0], dy = y - clock[1];
			if (Math.sqrt(dx * dx + dy * dy) <= clock[2] + 2) {
				clickedClock = true;
				clickedSignal = this.signals[stn][dir];
			}
		}, this);
	}, this);

	if (clickedSignal && desiredAspect && desiredAspect != clickedSignal.aspect) {
		this.changeSignal(clickedSignal.station, clickedSignal.direction, desiredAspect);
	} else if (clickedSignal && clickedClock) {
		this.toggleClockMenu(clickedSignal);
	} else {
		// Nothing clickable was clicked; close train and clock menus
		if (this.activeSignal) {
			this.toggleClockMenu(this.activeSignal);
		}
		if (this.activeStrip) {
			this.toggleStrip(this.activeStrip);
		}
	}
};
