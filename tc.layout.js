/*
 * Layout section of the Transit Control display.
 * This is only separate because tc.js would be too long otherwise.
 * (c) Sammi de Guzman, 2016.
 */

TransitControl.prototype.populateStations = function () {
	if (this.line == 1) {
		this.names = {
			FIN: "Finch",
			NYC: "North York Centre",
			SHP: "Sheppard-Yonge",
			YKM: "York Mills",
			YKMC: "York Mills Centre Track",
			LAW: "Lawrence",
			EGL: "Eglinton",
			EGLP: "Eglinton Pocket",
			DAV: "Davisville",
			DAVY: "Davisville 3rd Platform",
			STC: "St Clair",
			SUM: "Summerhill",
			ROS: "Rosedale",
			BLO: "Bloor-Yonge",
			WEL: "Wellesley",
			COL: "College",
			DUN: "Dundas",
			QUN: "Queen",
			KNG: "King",
			UNI: "Union",
			UNIC: "Union Centre Track",
			STA: "St Andrew",
			OSG: "Osgoode",
			OSGP: "Osgoode Pocket",
			STP: "St Patrick",
			QPK: "Queen's Park",
			MUS: "Museum",
			STG: "St George",
			SPA: "Spadina",
			DUP: "Dupont",
			SCW: "St Clair West",
			SCWC: "St Clair West Centre Track",
			EGW: "Eglinton West",
			GCN: "Glencairn",
			LWWC: "Lawrence West Centre Track",
			LWW: "Lawrence West",
			YKD: "Yorkdale",
			WIL: "Wilson",
			WILY: "Wilson Hostler",
			DWN: "Downsview"
		};
		this.stations = [
			"FIN", "NYC", "SHP", "YKM", "LAW", "EGL", "DAV", "STC",
			"SUM", "ROS", "BLO", "WEL", "COL", "DUN", "QUN", "KNG",
			"UNI", "STA", "OSG", "STP", "QPK", "MUS", "STG", "SPA",
			"DUP", "SCW", "EGW", "GCN", "LWW", "YKD", "WIL", "DWN"
		];
		this.termini = ["DWN", "FIN"];
		this.otherCodes = {
			"YKMC": "3a",
			"EGLP": "5a",
			"DAVY": "6a",
			"UNIC": "16a",
			"OSGP": "18a",
			"SCWC": "25a",
			"LWWC": "27a",
			"WILY": "30a"
		};
		this.enroutes = [
			{
				0:  ["FIN", "FIN.S591"],
				2:  ["SHP", "YKM.X60"],
				3:  ["YKM", "YKM.X14"],
				4:  ["LAW", "EGL.X66"],
				24: ["DUP", "DUP.SP60"],
				25: ["SCW", "SCW.X6", "SCW.SP122"],
				30: ["WIL", "WIL.X76"]
			},
			{ // enroutes for Finch-bound
			}
		];
	} else if (this.line == 2) {
		this.names = {
			KEN: "Kennedy",
			WAR: "Warden",
			VPK: "Victoria Park",
			MST: "Main Street",
			WDB: "Woodbine",
			COX: "Coxwell",
			GRN: "Greenwood",
			GRNY: "Greenwood Yard",
			DON: "Donlands",
			PAP: "Pape",
			CHE: "Chester",
			CHEC: "Chester Centre Track",
			BRD: "Broadview",
			CFK: "Castle Frank",
			SHE: "Sherbourne",
			YNG: "Bloor-Yonge",
			BAY: "Bay",
			STG: "St George",
			SPA: "Spadina",
			BAT: "Bathurst",
			CHR: "Christie",
			OSSC: "Ossington Centre Track",
			OSS: "Ossington",
			DUF: "Dufferin",
			LAN: "Lansdowne",
			DNW: "Dundas West",
			VNCY: "Vincent Yard",
			KEL: "Keele",
			HPK: "High Park",
			RUN: "Runnymede",
			JAN: "Jane",
			OML: "Old Mill",
			RYK: "Royal York",
			ISLC: "Islington Centre Track",
			ISL: "Islington",
			KIP: "Kipling"
		};
		this.stations = [
			"KEN", "WAR", "VPK", "MST", "WDB", "COX", "GRN", "DON",
			"PAP", "CHE", "BRD", "CFK", "SHE", "YNG", "BAY", "STG",
			"SPA", "BAT", "CHR", "OSS", "DUF", "LAN", "DNW", "KEL",
			"HPK", "RUN", "JAN", "OML", "RYK", "ISL", "KIP"
		];
		this.termini = ["KIP", "KEN"];
		this.otherCodes = {
			"GRNY": "6a",
			"CHEC": "9a",
			"OSSC": "19a",
			"VNCY": "23a",
			"ISLC": "29a"
		};
		this.enroutes = [
			{
				0: ["KEN", "WAR.X34"],
				1: ["WAR", "VPK.X18"],
				28: ["RYK", "ISL.X48"],
				29: ["ISL", "KIP.X18"]
			},
			{ // enroutes for Kennedy-bound
			}
		];
	} else if (this.line == 4) {
		this.names = {
			SHY: "Sheppard-Yonge",
			BYV: "Bayview",
			BSS: "Bessarion",
			LES: "Leslie",
			DML: "Don Mills"
		};
		this.stations = [
			"SHY", "BYV", "BSS", "LES", "DML"
		];
		this.termini = ["DML", "SHY"];
		this.otherCodes = {};
		this.enroutes = [ {}, {} ]; // no enroutes
	}
};

TransitControl.prototype.populateLayout = function () {
	if (this.line == 1) {
		this.layout.stationCenters = [
			[100, 150], // Finch
			[220, 150],
			[280, 150],
			[400, 150],
			[520, 150],
			[640, 150], // Eglinton
			[700, 150],
			[760, 150],
			[820, 150],
			[880, 150],
			[940, 150], // Bloor
			[ 70, 350],
			[130, 350],
			[190, 350],
			[250, 350],
			[310, 350],
			[370, 350], // Union
			[440, 350],
			[510, 350],
			[570, 350],
			[630, 350],
			[690, 350],
			[750, 350], // St George
			[810, 350],
			[870, 350],
			[ 70, 550], // St Clair West
			[250, 550],
			[310, 550],
			[410, 550],
			[470, 550],
			[530, 550],
			[650, 550]  // Downsview
		];
		this.layout.platforms = [
			0, 1, 0, 0, 0, 0, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1,
			2, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 4, 0
		];
		this.layout.tracks = [
			// Finch to Bloor
			[[ 50, 141], [985, 141]],
			[[ 50, 159], [985, 159]],

			// Finch pocket
			[[ 50, 150], [ 80, 150], [ 85, 141]],
			[[ 80, 150], [ 85, 159]],
			// Finch crossover
			[[115, 141], [125, 159]],
			[[115, 159], [125, 141]],
			// Sheppard crossover
			[[295, 141], [305, 159]],
			[[295, 159], [305, 141]],
			// York Mills center
			[[415, 141], [420, 150], [480, 150], [485, 159]],
			[[415, 159], [420, 150]],
			[[480, 150], [485, 141]],
			// Lawrence crossover
			[[535, 141], [545, 159]],
			[[535, 159], [545, 141]],
			// Eglinton pocket
			[[605, 159], [600, 150], [570, 150]],
			[[605, 141], [600, 150]],
			// Eglinton crossover
			[[655, 141], [665, 159]],
			[[655, 159], [665, 141]],
			// Davisville yard
			[[669, 141], [677, 159], [685, 177], [715, 177], [723, 159], [731, 141]],
			// St Clair crossover
			[[775, 141], [785, 159]],
			[[775, 159], [785, 141]],
			// Bloor crossover
			[[915, 141], [925, 159]],
			[[915, 159], [925, 141]],

			// Wellesley to Dupont
			[[ 50, 341], [985, 341]],
			[[ 50, 359], [985, 359]],

			// College crossover
			[[145, 341], [155, 359]],
			[[145, 359], [155, 341]],
			// King crossover
			[[285, 341], [295, 359]],
			[[285, 359], [295, 341]],
			// Union crossover
			[[345, 341], [355, 359]],
			// Union center
			[[385, 341], [390, 350], [420, 350], [425, 359]],
			[[385, 359], [390, 350]],
			[[420, 350], [425, 341]],
			// Osgoode pocket
			[[460, 350], [490, 350], [495, 359]],
			[[490, 350], [495, 341]],
			// St George crossover
			[[725, 341], [735, 359]],
			[[725, 359], [735, 341]],
			// Spadina crossover
			[[825, 341], [835, 359]],
			[[825, 359], [835, 341]],

			// St Clair West to Downsview
			[[ 50, 541], [700, 541]],
			[[ 50, 559], [700, 559]],

			// St Clair West center
			[[ 85, 541], [ 90, 550], [150, 550], [155, 559]],
			[[ 85, 559], [ 90, 550]],
			[[150, 550], [155, 541]],
			// Lawrence West center
			[[355, 541], [360, 550], [390, 550], [395, 559]],
			[[355, 559], [360, 550]],
			[[390, 550], [395, 541]],
			// Wilson crossover
			[[505, 541], [515, 559]],
			[[505, 559], [515, 541]],
			// Wilson hostler
			[[545, 559], [555, 541], [565, 523], [595, 523], [605, 541], [615, 559]],
			// Downsview crossover
			[[625, 541], [635, 559]],
			[[625, 559], [635, 541]],
			// Downsview tail tracks
			[[665, 541], [670, 550], [700, 550]],
			[[665, 559], [670, 550]]
		];
		this.layout.trainLocations = [
			// trainLocations [dir] [stn] [enroutes] [departed]
			[	// DOWNSVIEW (down):
				[[[100, 159], [130, 159]],	// Finch
				 [[160, 159], [190, 159]]],
				[[[220, 159], [250, 159]]],
				[[[280, 159], [310, 159]],
				 [[340, 159], [370, 159]]],
				[[[400, 159], [430, 159]],
				 [[460, 159], [490, 159]]],
				[[[520, 159], [550, 159]],
				 [[580, 159], [610, 159]]],
				[[[640, 159], [670, 159]]],	// Eglinton
				[[[700, 159], [730, 159]]],
				[[[760, 159], [790, 159]]],
				[[[820, 159], [850, 159]]],
				[[[880, 159], [910, 159]]],
				[[[940, 159], [970, 159]]],	// Bloor
				[[[ 70, 359], [100, 359]]],
				[[[130, 359], [160, 359]]],
				[[[190, 359], [220, 359]]],
				[[[250, 359], [280, 359]]],
				[[[310, 359], [340, 359]]],
				[[[370, 359], [405, 359]]],	// Union
				[[[440, 359], [475, 359]]],
				[[[510, 359], [540, 359]]],
				[[[570, 359], [600, 359]]],
				[[[630, 359], [660, 359]]],
				[[[690, 359], [720, 359]]],
				[[[750, 359], [780, 359]]],	// St George
				[[[810, 359], [840, 359]]],
				[[[870, 359], [900, 359]],
				 [[930, 359], [960, 359]]],
				[[[ 70, 559], [100, 559]],	// St Clair West
				 [[130, 559], [160, 559]],
				 [[190, 559], [220, 559]]],
				[[[250, 559], [280, 559]]],
				[[[310, 559], [360, 559]]],
				[[[410, 559], [440, 559]]],
				[[[470, 559], [500, 559]]],
				[[[530, 559], [560, 559]],
				 [[590, 559], [620, 559]]],
				[[[650, 559]]]				// Downsview
			],
			[	// FINCH (up):
				[[[100, 141]]],				// Finch
				[[[220, 141], [160, 141]]],
				[[[280, 141], [250, 141]]],
				[[[400, 141], [340, 141]]],
				[[[520, 141], [460, 141]]],
				[[[640, 141], [580, 141]]],	// Eglinton
				[[[700, 141], [670, 141]]],
				[[[760, 141], [730, 141]]],
				[[[820, 141], [790, 141]]],
				[[[880, 141], [850, 141]]],
				[[[940, 141], [910, 141]]],	// Bloor
				[[[ 70, 341], [970, 141]]],
				[[[130, 341], [100, 341]]],
				[[[190, 341], [160, 341]]],
				[[[250, 341], [220, 341]]],
				[[[310, 341], [280, 341]]],
				[[[370, 341], [340, 341]]],	// Union
				[[[440, 341], [405, 341]]],
				[[[510, 341], [475, 341]]],
				[[[570, 341], [540, 341]]],
				[[[630, 341], [600, 341]]],
				[[[690, 341], [660, 341]]],
				[[[750, 341], [720, 341]]],	// St George
				[[[810, 341], [780, 341]]],
				[[[870, 341], [840, 341]]],
				[[[ 70, 541], [930, 341]]],	// St Clair West
				[[[250, 541], [155, 541]]],
				[[[310, 541], [280, 541]]],
				[[[410, 541], [360, 541]]],
				[[[470, 541], [440, 541]]],
				[[[530, 541], [500, 541]]],
				[[[650, 541], [590, 541]]]	// Downsview
			]
		];
		this.layout.storageTrackLocations = {
			"3a":  [450, 150],
			"5a":  [585, 150],
			"6a":  [700, 177],
			"16a": [405, 350],
			"18a": [475, 350],
			"25a": [120, 550],
			"28a": [375, 550],
			"30a": [580, 523]
		};
		this.layout.signalsToStations = [
			 6,  8,  9, 11, 13, 15, 16, 17,
			18, 19, 20, 21, 22, 23, 24, 25,
			26, 27, 28, 29, 30, 31, 32, 33,
			34, 36, 39, 40, 41, 42, 43, 45
		];
		this.layout.enrouteToSignal = [
			[6, 7], [8], [9, 10], [11, 12], [13, 14],
			[15], [16], [17], [18], [19], [20], [21],
			[22], [23], [24], [25], [26], [27], [28],
			[29], [30], [31], [32], [33], [34, 35],
			[36, 37, 38], [39], [40], [41], [42], [43, 44], [45]
		];
		this.layout.signals = {
			6: {
				label: "FIN",
				signals: [[115, 168], [85, 132]],
				selectors: [[85, 210], null]
			},
			7: {
				label: "FIN.S591",
				signals: [[175, 168], null],
			},
			8: {
				label: "NYC",
				signals: [[235, 168], [205, 132]],
				selectors: [null, [190, 80]]
			},
			9: {
				label: "SHP",
				signals: [[295, 168], [265, 132]],
				selectors: [[270, 210], [270, 80]]
			},
			10: {
				label: "YKM.X60",
				signals: [[355, 168], [325, 132]],
				selectors: [null, null]
			},
			11: {
				label: "YKM",
				signals: [[415, 168], [385, 132]],
				selectors: [[390, 210], [390, 80]]
			},
			81: {
				label: "YKMC",
				signals: [[485, 150], null],
				selectors: [[470, 50], null]
			},
			12: {
				label: "YKM.X14",
				signals: [[475, 168], [445, 132]],
				selectors: [[470, 240], null]
			},
			13: {
				label: "LAW",
				signals: [[535, 168], [505, 132]],
				selectors: [[520, 210], [520, 80]]
			},
			14: {
				label: "EGL.X66",
				signals: [[595, 168], [565, 132]],
				selectors: [[600, 240], null]
			},
			82: {
				label: "EGLP",
				signals: [[605, 150], null],
				selectors: [[580, 110], null]
			},
			15: {
				label: "EGL",
				signals: [[655, 168], [625, 132]],
				selectors: [[640, 210], [608, 80]]
			},
			16: {
				label: "DAV",
				signals: [[715, 168], [685, 132]],
				selectors: [[718, 210], [685, 80]]
			},
			83: {
				label: "DAVY",
				signals: [[715, 183], null],
				selectors: [[700, 240], null]
			},
			17: {
				label: "STC",
				signals: [[775, 168], [745, 132]]
			},
			18: {
				label: "SUM",
				signals: [[835, 168], [805, 132]]
			},
			19: {
				label: "ROS",
				signals: [[895, 168], [865, 132]],
				selectors: [[864, 210], null]
			},
			20: {
				label: "BLO",
				signals: [[955, 168], [925, 132]],
				selectors: [[935, 210], [925, 80]]
			},
			21: {
				label: "WEL",
				signals: [[85, 368], [55, 332]],
				selectors: [null, [64, 280]]
			},
			22: {
				label: "COL",
				signals: [[145, 368], [115, 332]]
			},
			23: {
				label: "DUN",
				signals: [[205, 368], [175, 332]]
			},
			24: {
				label: "QUN",
				signals: [[265, 368], [235, 332]],
				selectors: [null, null]
			},
			25: {
				label: "KNG",
				signals: [[325, 368], [295, 332]],
				selectors: [null, [285, 280]]
			},
			26: {
				label: "UNI",
				signals: [[385, 368], [355, 332]],
				selectors: [[358, 410], [358, 280]]
			},
			84: {
				label: "UNIC",
				signals: [[425, 350], null],
				selectors: [[422, 440], null]
			},
			27: {
				label: "STA",
				signals: [[455, 368], [425, 332]],
				selectors: [[430, 410], [430, 280]]
			},
			28: {
				label: "OSG",
				signals: [[525, 368], [495, 332]]
			},
			85: {
				label: "OSGP",
				signals: [[495, 350], null],
				selectors: [[507, 440], null]
			},
			29: {
				label: "STP",
				signals: [[585, 368], [555, 332]]
			},
			30: {
				label: "QPK",
				signals: [[645, 368], [615, 332]],
				selectors: [null, [600, 280]]
			},
			31: {
				label: "MUS",
				signals: [[705, 368], [675, 332]],
				selectors: [[670, 410], [670, 280]]
			},
			32: {
				label: "STG",
				signals: [[765, 368], [735, 332]],
				selectors: [[745, 410], [745, 280]]
			},
			33: {
				label: "SPA",
				signals: [[825, 368], [795, 332]],
				selectors: [[816, 410], [816, 280]]
			},
			34: {
				label: "DUP",
				signals: [[885, 368], [855, 332]]
			},
			35: {
				label: "DUP.SP60",
				signals: [[945, 368], null]
			},
			36: {
				label: "SCW",
				signals: [[85, 568], [55, 532]],
				selectors: [[65, 610], [65, 480]]
			},
			86: {
				label: "SCWC",
				signals: [[155, 550], null],
				selectors: [[145, 480]]
			},
			37: {
				label: "SCW.X6",
				signals: [[145, 568], [115, 532]],
				selectors: [[145, 610], null]
			},
			38: {
				label: "SCW.SP122",
				signals: [[205, 568], [175, 532]]
			},
			39: {
				label: "EGW",
				signals: [[265, 568], [235, 532]]
			},
			40: {
				label: "GCN",
				signals: [[325, 568], [295, 532]]
			},
			87: {
				label: "LWWC",
				signals: [[395, 550], null],
				selectors: [[365, 610], null]
			},
			41: {
				label: "LWW",
				signals: [[425, 568], [395, 532]],
				selectors: [null, [398, 480]]
			},
			42: {
				label: "YKD",
				signals: [[485, 568], [455, 532]],
				selectors: [[440, 610], null]
			},
			43: {
				label: "WIL",
				signals: [[545, 568], [515, 532]],
				selectors: [[514, 610], [496, 480]]
			},
			88: {
				label: "WILY",
				signals: [null, [563, 510]],
				selectors: [null, [578, 480]]
			},
			44: {
				label: "WIL.X76",
				signals: [[605, 568], [575, 532]],
				selectors: [[588, 610], null]
			},
			45: {
				label: "DWN",
				signals: [[665, 568], [635, 532]],
				selectors: [null, [652, 480]]
			},
		};
	} else if (this.line == 2) {
		this.layout.stationCenters = [
			[100, 150], // Kennedy
			[220, 150],
			[340, 150],
			[400, 150],
			[460, 150],
			[520, 150],
			[580, 150], // Greenwood
			[660, 150],
			[720, 150],
			[780, 150],
			[880, 150],
			[940, 150],
			[ 70, 350],
			[130, 350], // Yonge
			[190, 350],
			[250, 350],
			[310, 350],
			[370, 350],
			[430, 350],
			[530, 350], // Ossington
			[590, 350],
			[650, 350],
			[710, 350],
			[790, 350], // Keele
			[850, 350],
			[910, 350],
			[970, 350],
			[100, 550],
			[160, 550],
			[310, 550],
			[440, 550]	// Kipling
		];
		this.layout.platforms = [
			0, 0, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 0, 0, 0,
			1, 1, 1, 1, 1, 1, 1, 1,
			1, 1, 1, 1, 1, 0, 0
		];
		this.layout.tracks = [
			// Kennedy to Castle Frank
			[[ 50, 141], [985, 141]],
			[[ 50, 159], [985, 159]],

			// Kennedy crossover
			[[115, 141], [125, 159]],
			[[115, 159], [125, 141]],
			// Warden crossover
			[[235, 141], [245, 159]],
			[[235, 159], [245, 141]],
			// Victoria Park crossover
			[[315, 141], [325, 159]],
			[[315, 159], [325, 141]],
			// Woodbine crossover
			[[475, 141], [485, 159]],
			[[475, 159], [485, 141]],
			// Greenwood yard
			[[600, 159], [610, 141], [620, 123], [630, 105], [660, 105]],
			[[615, 132], [620, 141]],
			[[605, 150], [610, 159]],
			// Chester center
			[[795, 141], [800, 150], [830, 150], [835, 159]],
			[[795, 159], [800, 150]],
			[[830, 150], [835, 141]],

			// Sherbourne to Jane
			[[ 50, 341], [985, 341]],
			[[ 50, 359], [985, 359]],

			// St George crossover
			[[225, 341], [235, 359]],
			[[225, 359], [235, 341]],
			// Ossington center
			[[475, 341], [480, 350], [510, 350], [515, 359]],
			[[475, 359], [480, 350]],
			[[510, 350], [515, 341]],
			// Keele crossover
			[[765, 341], [775, 359]],
			[[765, 359], [775, 341]],
			// Vincent yard
			[[765, 341], [755, 323], [745, 305], [715, 305]],
			// Jane crossover
			[[945, 341], [955, 359]],
			[[945, 359], [955, 341]],

			// Old Mill to Kipling
			[[ 50, 541], [490, 541]],
			[[ 50, 559], [490, 559]],

			// Islington center
			[[235, 541], [240, 550], [270, 550], [275, 559]],
			[[235, 559], [240, 550]],
			[[270, 550], [275, 541]],
			// Islington crossover
			[[285, 541], [295, 559]],
			[[285, 559], [295, 541]],
			// Kipling crossover
			[[415, 541], [425, 559]],
			[[415, 559], [425, 541]]
		];
		this.layout.trainLocations = [
			[	// WESTBOUND:
				[[[100, 159], [130, 159]],	// Kennedy
				 [[160, 159], [190, 159]]],
				[[[220, 159], [250, 159]],
				 [[280, 159], [310, 159]]],
				[[[340, 159], [370, 159]]],
				[[[400, 159], [430, 159]]],
				[[[460, 159], [490, 159]]],
				[[[520, 159], [550, 159]]],
				[[[580, 159], [610, 159]]], // Greenwood
				[[[660, 159], [690, 159]]],
				[[[720, 159], [750, 159]]],
				[[[780, 159], [830, 159]]],
				[[[880, 159], [910, 159]]],
				[[[940, 159], [970, 159]]],
				[[[ 70, 359], [100, 359]]],
				[[[130, 359], [160, 359]]], // Yonge
				[[[190, 359], [220, 359]]],
				[[[250, 359], [280, 359]]],
				[[[310, 359], [340, 359]]],
				[[[370, 359], [400, 359]]],
				[[[430, 359], [480, 359]]],
				[[[530, 359], [560, 359]]], // Ossington
				[[[590, 359], [620, 359]]],
				[[[650, 359], [680, 359]]],
				[[[710, 359], [740, 359]]],
				[[[790, 359], [820, 359]]], // Keele
				[[[850, 359], [880, 359]]],
				[[[910, 359], [940, 359]]],
				[[[970, 359], [ 70, 559]]],
				[[[100, 559], [130, 559]]],
				[[[160, 559], [190, 559]],
				 [[220, 559], [270, 559]]],
				[[[310, 559], [340, 559]],
				 [[370, 559], [410, 559]]],
				[[[440, 559]]]				// Kipling
			],
			[	// EASTBOUND:
				[], // Kennedy
				[],
				[],
				[],
				[],
				[],
				[], // Greenwood
				[],
				[],
				[],
				[],
				[],
				[],
				[], // Yonge
				[],
				[],
				[],
				[],
				[],
				[], // Ossington
				[],
				[],
				[],
				[], // Keele
				[],
				[],
				[],
				[],
				[],
				[],
				[[[440, 541]]]  // Kipling
			]
		];
		this.layout.storageTrackLocations = {
			"6a": [645, 105],
			"9a": [815, 150],
			"19a": [495, 350],
			"23a": [730, 305],
			"29a": [255, 550]
		};
		this.layout.signalsToStations = [
			46, 48, 50, 51, 52, 53, 54, 55,
			56, 57, 58, 59, 60, 61, 62, 63,
			64, 65, 66, 67, 68, 69, 70, 71,
			72, 73, 74, 75, 76, 78, 80
		];
		this.layout.enrouteToSignal = [
			[46, 47], [48, 49], [50], [51], [52],
			[53], [54], [55], [56], [57], [58], [59],
			[60], [61], [62], [63], [64], [65], [66],
			[67], [68], [69], [70], [71], [72], [73],
			[74], [75], [76, 77], [78, 79], [80]
		];
		this.layout.signals = {
			46: {
				label: "KEN",
				signals: [[115, 168], [85, 132]],
				selectors: [[90, 210], null]
			},
			47: {
				label: "WAR.X34",
				signals: [[175, 168], null],
				selectors: [[160, 210], null]
			},
			48: {
				label: "WAR",
				signals: [[235, 168], [205, 132]],
				selectors: [[230, 210], null]
			},
			49: {
				label: "VPK.X18",
				signals: [[295, 168], [265, 132]],
				selectors: [[300, 210], null]
			},
			50: {
				label: "VPK",
				signals: [[355, 168], [325, 132]]
			},
			51: {
				label: "MST",
				signals: [[415, 168], [385, 132]],
				selectors: [[390, 210], null]
			},
			52: {
				label: "WDB",
				signals: [[475, 168], [445, 132]],
				selectors: [[450, 210], null]
			},
			53: {
				label: "COX",
				signals: [[535, 168], [505, 132]]
			},
			54: {
				label: "GRN",
				signals: [[595, 168], [565, 132]],
				selectors: [[570, 210], null]
			},
			55: {
				label: "DON",
				signals: [[675, 168], [645, 132]]
			},
			56: {
				label: "PAP",
				signals: [[735, 168], [705, 132]]
			},
			57: {
				label: "CHE",
				signals: [[795, 168], [765, 132]],
				selectors: [[770, 210], null]
			},
			89: {
				label: "CHEC",
				signals: [[835, 150], null],
				selectors: [[850, 210], null]
			},
			58: {
				label: "BRD",
				signals: [[895, 168], [865, 132]]
			},
			59: {
				label: "CFK",
				signals: [[955, 168], [925, 132]]
			},
			60: {
				label: "SHE",
				signals: [[85, 368], [55, 332]]
			},
			61: {
				label: "YNG",
				signals: [[145, 368], [115, 332]],
				selectors: [[105, 410], null]
			},
			62: {
				label: "BAY",
				signals: [[205, 368], [175, 332]],
				selectors: [[180, 410], null]
			},
			63: {
				label: "STG",
				signals: [[265, 368], [235, 332]],
				selectors: [[255, 410], null]
			},
			64: {
				label: "SPA",
				signals: [[325, 368], [295, 332]]
			},
			65: {
				label: "BAT",
				signals: [[385, 368], [355, 332]]
			},
			66: {
				label: "CHR",
				signals: [[445, 368], [415, 332]],
				selectors: [[420, 410], null]
			},
			90: {
				label: "OSSC",
				signals: [[515, 350], null],
				selectors: [[510, 410], null]
			},
			67: {
				label: "OSS",
				signals: [[545, 368], [515, 332]]
			},
			68: {
				label: "DUF",
				signals: [[605, 368], [575, 332]]
			},
			69: {
				label: "LAN",
				signals: [[665, 368], [635, 332]]
			},
			70: {
				label: "DNW",
				signals: [[725, 368], [695, 332]],
				selectors: [[700, 410], null]
			},
			71: {
				label: "KEL",
				signals: [[805, 368], [775, 332]],
				selectors: [[780, 410], null]
			},
			72: {
				label: "HPK",
				signals: [[865, 368], [835, 332]]
			},
			73: {
				label: "RUN",
				signals: [[925, 368], [895, 332]]
			},
			74: {
				label: "JAN",
				signals: [[985, 368], [955, 332]]
			},
			75: {
				label: "OML",
				signals: [[115, 568], [85, 532]]
			},
			76: {
				label: "RYK",
				signals: [[175, 568], [145, 532]],
				selectors: [[150, 610], null]
			},
			77: {
				label: "ISL.X48",
				signals: [[235, 568], [205, 532]],
				selectors: [[230, 610], null]
			},
			91: {
				label: "ISLC",
				signals: [[275, 550], null],
				selectors: [[245, 505], null]
			},
			78: {
				label: "ISL",
				signals: [[325, 568], [295, 532]],
				selectors: [[310, 610], null]
			},
			79: {
				label: "KIP.X18",
				signals: [[385, 568], [355, 532]],
				selectors: [[390, 610], null]
			},
			80: {
				label: "KIP",
				signals: [[455, 568], [425, 532]]
			}
		};
	} else if (this.line == 4) {
		this.layout.stationCenters = [
			[100, 150], // Sheppard-Yonge
			[220, 150],
			[340, 150],
			[460, 150],
			[580, 150]  // Don Mills
		];
		this.layout.platforms = [5, 0, 0, 0, 0];
		this.layout.tracks = [
			// Sheppard-Yonge to Don Mills
			[[ 50, 141], [610, 141]],
			[[ 50, 159], [610, 159]],

			// Sheppard-Yonge crossover
			[[115, 141], [125, 159]],
			[[115, 159], [125, 141]],
			// Bayview crossover
			[[235, 141], [245, 159]],
			[[235, 159], [245, 141]],
			// Don Mills crossover
			[[555, 141], [565, 159]],
			[[555, 159], [565, 141]]
		];
		this.layout.trainLocations = [
			[	// EASTBOUND:
				[[[100, 159], [160, 159]]],	// Sheppard-Yonge
				[[[220, 159], [280, 159]]],
				[[[340, 159], [400, 159]]],
				[[[460, 159], [520, 159]]],
				[[[580, 159]]]				// Don Mills
			],
			[	// WESTBOUND:
				[[[100, 141]]],				// Sheppard-Yonge
				[[[220, 141], [160, 141]]],
				[[[340, 141], [280, 141]]],
				[[[460, 141], [400, 141]]],
				[[[580, 141], [520, 141]]]	// Don Mills
			]
		];
		this.layout.signalsToStations = [1, 2, 3, 4, 5];
		this.layout.signals = {
			1: {
				label: "SHY",
				signals: [[115, 168], [85, 132]],
				selectors: [[90, 210], [90, 80]]
			},
			2: {
				label: "BYV",
				signals: [[235, 168], [205, 132]],
				selectors: [[210, 210], [210, 80]]
			},
			3: {
				label: "BSS",
				signals: [[355, 168], [325, 132]],
				selectors: [[330, 210], [330, 80]]
			},
			4: {
				label: "LES",
				signals: [[475, 168], [445, 132]],
				selectors: [[450, 210], [450, 80]]
			},
			5: {
				label: "DML",
				signals: [[595, 168], [565, 132]],
				selectors: [[570, 210], [570, 80]]
			},
		};
	}
};
