/*
 * Data module for the Transit Control display.
 * Only the functions here will interact directly with the website.
 * (c) Sammi de Guzman, 2016.
 */

$ = {
	get: function (url, data, success, error) {
		reqwest({
			url: url,
			type: "json",
			method: "get",
			data: data || {},
			success: success || function () {},
			error: error || function () {}
		});
	},
	post: function (url, data, success, error) {
		reqwest({
			url: url,
			type: "json",
			method: "post",
			data: data || {},
			success: success || function () {},
			error: error || function () {}
		});
	}
};

TransitControl.prototype.reloadData = function (success, error) {
	var self = this;
	success = success || function () {};
	error = error || function () {};
	$.get("controldata.php", {
		t: new Date().getTime()
	}, function (data) {
		self.update(data);
		success(data);
	}, error);
};

TransitControl.prototype.updateDataControl = function (name, trains, v1, v2, v3) {
	var self = this;
	$.post("controlupdate.php",
		{name: name, numtrains: trains, value1: v1, value2: v2, value3: v3},
		function () { self.reloadData(); });
};

TransitControl.prototype.resetTrip = function (run) {
	this.updateDataControl("tripreset", 1, run);
};

TransitControl.prototype.resetAlarm = function (run) {
	this.updateDataControl("alarmreset", 1, run);
};

TransitControl.prototype.moveTrain = function (run, stnCode, direction) {
	this.updateDataControl("relocate", 1, run, stnCode, direction);
};

TransitControl.prototype.changeSignal = function (stnCode, direction, aspect) {
	this.updateDataControl("setaspect", 1, stnCode, direction, aspect);
};

TransitControl.prototype.setSignalTime = function (stnCode, direction, time) {
	this.updateDataControl("settime", 1, stnCode, direction, time);
};

TransitControl.prototype.cancelSignalTime = function (stnCode, direction) {
	this.updateDataControl("canceltime", 1, stnCode, direction);
};

TransitControl.prototype.logoff = function (badge) {
	this.updateDataControl("logoff", 1, badge);
	setInterval(function () {
		window.location.href = "/controlview_login.php";
	}, 1000);
};
