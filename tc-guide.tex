\documentclass[11pt,letterpaper]{article}
\usepackage[margin=1in]{geometry}
\usepackage{afterpage}
\usepackage{pagecolor}
\usepackage{xcolor}
\usepackage{graphicx}
\usepackage{setspace}
\usepackage{fancyhdr}
\usepackage{mdframed}
\usepackage[colorlinks=true,linkcolor=red]{hyperref}
\usepackage{fontspec}
\usepackage{array}
\usepackage{tikz}
\definecolor{tcgray}{gray}{0.1333}
\definecolor{tclightgray}{gray}{0.2667}
\definecolor{tclayout}{gray}{0.375}
\definecolor{tcred}{HTML}{E08080}
\definecolor{tcorange}{HTML}{E08000}
\definecolor{tcyellow}{HTML}{E0E000}
\definecolor{tcgreen}{HTML}{5D987B}
\definecolor{tcblue}{HTML}{88CCFF}
\definecolor{sigred}{HTML}{CC0000}
\definecolor{sigamber}{HTML}{FFA500}
\definecolor{siggreen}{HTML}{228822}
\setmainfont{Roboto}[UprightFont={* Light},BoldFont={* Medium}]
\setlength{\parskip}{\baselineskip}
\setlength{\parindent}{0pt}
\newcommand{\tc}{\textbf{Transit}Control}
\newcommand{\TODO}[1]{\begin{mdframed}[linecolor=red]\color{red}TODO: #1\end{mdframed}}
\newfontfamily{\monoOne}{monoOne}
\newcommand{\mono}[1]{{\monoOne #1}}
\newcommand{\status}[2]{\colorbox{tcgray}{\color{tc#1}\mono{\textit{#2}}}}
\newcommand{\location}[1]{\colorbox{tcgray}{\color{white}\mono{#1}}}
\newcommand{\signal}[2]{
	\begin{tikzpicture}
	\fill[sig#1] (0,0.3) circle (0.3em);
	\fill[sig#2] (0,0) circle (0.3em);
	\end{tikzpicture}
}
\newcommand{\doublered}{\signal{red}{red}}
\newcommand{\doublegreen}{\signal{green}{green}}
\newcommand{\doubleamber}{\signal{amber}{amber}}
\newcommand{\ambergreen}{\signal{amber}{green}}
\newcommand{\autosignal}[1]{
	\begin{tikzpicture}
	\fill[sig#1] (0,0.3) circle (0.3em);
	\end{tikzpicture}
}
\begin{document}
\begin{titlepage}
\pagecolor{tcgray}
\null
\vspace{\stretch{1}}
\includegraphics[scale=2.5]{logo.eps}\\
\vspace{2ex}\\
\textcolor{white}{\LARGE Controller's Guide}
\end{titlepage}
\pagecolor{white}
\thispagestyle{empty}

\spacing{1.5}
\large

\tc{} is the newest addition to the Virtual TTC Academy website.
It makes a few design improvements over the existing Tower Control display:

\begin{itemize}
\item
	more information is displayed that was previously only on other pages;
\item
	the display updates every \textit{three} seconds instead of ten;
\item
	all actions update the display automatically so there is no need to wait
	until the next update to see the changes;
\item
	the display loads any new data in the background so the page no longer
	needs to reload and the changes show seamlessly.
\end{itemize}

This Controller's Guide helps tower controllers make the best use out of
\tc, and explains the differences between it and the Tower Control display.
It does not assume any familiarity with the Tower Control display, so it
can also be useful for training new tower controllers who have not
previously used the old system.

\vspace{\stretch{1}}

\normalsize
\spacing{1.2}
\textbf{Sammi de Guzman} \\
Head of IT, T2P Films \\
\vspace{2em} \\
\tc{} Controller's Guide \\
Version 1 \\
September 1, 2016 \\
\vspace{2em} \\
\includegraphics{academylogo.png}

\newpage
\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{0pt}
\fancyfoot[L]{\tc{} $\cdot$ Controller's Guide}
\fancyfoot[R]{\thepage}
\large
\tableofcontents

\newpage
\spacing{1.5}
\section{Control Display Layout}

\begin{center}
\includegraphics[width=\linewidth]{fullscreen.png}
\end{center}

Above is a screenshot of the control display. There are two main parts:

\begin{itemize}
\item
	The \textit{layout area} displays the layout of the entire line,
	including all the tracks and stations that comprise it, all the signals
	and which aspects they are currently showing, and the locations of all
	trains on the line.
\item
	The \textit{train list} contains more information on each individual
	train on the line. Below the train list the current TTC Academy time
	is also displayed, as well as a countdown to the next update and a
	button to log off the display.
\end{itemize}

\subsection{Layout Area}
The layout area displays the locations of all trains and the aspects of
all the signals on the line. A part of the layout for the Yonge line is
shown below:

\begin{center}
\includegraphics[width=0.75\linewidth]{display.png}
\end{center}

The aspects of all signals on the line are shown immediately next to the
platform. For those signals that can be changed by the controller,
signal selectors are shown \textit{below} the tracks for runs in the
original direction (towards Downsview, Kipling or Don Mills), and
\textit{above} the tracks for runs in the reverse direction (towards
Finch, Kennedy or Sheppard-Yonge). For storage tracks, the selector can
be shown either above or below the tracks.

The location of a train is indicated by its position on the layout. The
layout now differentiates between having departed a \textit{station} and
having departed an \textit{en-route signal}; the old system simply shows
a train as having departed. For example, a train that departed the
\textbf{York Mills X 14} signal is shown in a different location as one
that is approaching it.

A train's indicator is shown below, with its run number. The time below
the indicator is the time since it last arrived or departed a station or
en-route signal.

\begin{center}
\includegraphics[height=1.4in]{finch.png}
\end{center}

Similarly, each station has a time above or below it showing when a train
last arrived at or departed that station, in that direction:

\begin{center}
\includegraphics[height=1.4in]{headways.png}
\end{center}

The indicator may also be in a different color and have an exclamation mark
icon next to it if an issue arises on the train:

\begin{center}
\includegraphics[height=1.4in]{redalarm.png}
\end{center}

See \hyperref[train-status]{Train Statuses} for more information.

\subsection{Train List}
The train list shows information on all trains that have been dispatched
\textit{for which run numbers have been set}; trains without run numbers
(i.e. those where the run numbers are set to 0) will not be displayed
until a run number is assigned. The trains are sorted in the list by their
run number.

For each train, the following information is displayed:

\begin{center}
\includegraphics[width=3.5in]{train.png}
\end{center}

\textbf{Top row:}
\begin{itemize}
\item
	The train's \textit{run number}. This may be changed by the operator
	or tower during the run, but typically does not change.
\item
	The \textit{status} of the train. This part shows whether the train
	has arrived at or departed a station or en-route signal, whether the
	doors are open, or whether there are any issues on board the train.
	Next to the status is the time since the train last arrived at or
	departed the previous station or en-route.
	See \hyperref[train-status]{Train Statuses} for more information.
\item
	The name of the \textit{operator} that is currently logged onto the
	train on the website. If they are logged onto the simulator but not
	the website, this will remain blank.
\end{itemize}

\textbf{Bottom row:}
\begin{itemize}
\item
	The \textit{model} of the train that is being used in the simulator.
	This may not necessarily be the same as the type of train that was
	dispatched; in that case, this is the model of the train as given by
	the simulator plugin.
\item
	The \textit{location} of the train. The train is displayed as having
	arrived at a station or en-route signal, in which case the short code
	for that location is displayed, or having departed a station or en-route
	signal, in which case it is shown as being between the two locations.
	See \hyperref[train-location]{Train Locations} for more information.
\item
	The \textit{badge number and yard} of the operator that is currently
	logged onto the train on the website. If they are logged onto the
	simulator and not the website, this will remain blank, as with the operator
	above.

	The operator yards are shown as follows:
	\begin{itemize}
	\item \mono{(WIL)}: Wilson
	\item \mono{(DAV)}: Davisville
	\item \mono{(GRN)}: Greenwood
	\end{itemize}
\end{itemize}

\newpage
\section{Control Functions}

\subsection{Signal Aspects}
\subsubsection*{Signal Selectors}
\label{signal-selectors}
Only interlocking signals, or those with two color lights, can be controlled
by the tower. There are four possible aspects:

\begin{tabular}{cp{0.9\linewidth}}
\doublered & \textbf{STOP AND STAY} at the station or signal selected. \\
\doublegreen & \textbf{PROCEED at line speed} on the main line. The next two stations are clear
	and not set to a \autosignal{red} or \doublered. \\
\ambergreen & \textbf{PROCEED with caution} on the main line, the next signal
	is \autosignal{red} or \doublered. \\
\doubleamber & \textbf{PROCEED at line speed} on the diverging route. This is usually used at
	crossovers or to send trains into storage tracks. Once a train accepts a
	\doubleamber{} leaving a station before a crossover (e.g. Museum to St George),
	it will automatically be placed on the opposite platform once arrived. \\
\end{tabular}

Some interlocking signals may not have the \doubleamber{} aspect.

Signal selectors are shown next to each interlocking signal, allowing the controller
to select any of the possible aspects to display on the signal. The aspect that is
currently displayed on the signal is highlighted with a box. For example, below is the
southbound signal at Union set to \doublegreen:

\begin{center}
\begin{tikzpicture}
	\fill[tcgray] (-1.2,-0.5) -- (1.7,-0.5) -- (1.7,0.8) -- (-1.2,0.8);
	\node[text=tclayout,anchor=east] at (-0.15,0.15) {\small\mono{UNI}};
	\fill[sigred]   (0,0.3)   circle (0.3em);
	\fill[sigred]   (0,0)     circle (0.3em);
	\fill[siggreen] (0.4,0.3) circle (0.3em);
	\fill[siggreen] (0.4,0)   circle (0.3em);
	\fill[sigamber] (0.8,0.3) circle (0.3em);
	\fill[sigamber] (0.8,0)   circle (0.3em);
	\fill[sigamber] (1.2,0.3) circle (0.3em);
	\fill[siggreen] (1.2,0)   circle (0.3em);
	\draw[white,very thick] (0.2,-0.2) -- (0.6,-0.2) -- (0.6,0.5) -- (0.2,0.5) -- cycle;
\end{tikzpicture}
\end{center}

The controller can then click any of the other aspects to change it.
This change is reflected automatically on the corresponding signal on the control display.

\subsubsection*{Automatic Signals}
Automatic signals have only one color light and are automatically controlled
by the signal system based on the location of the train ahead and the next
signal. Since automatic signals cannot be controlled by the tower, they do
not have corresponding signal selectors.

\begin{tabular}{cp{0.9\linewidth}}
\autosignal{green} & \textbf{PROCEED at line speed}, the next two stations are clear. \\
\autosignal{amber} & \textbf{PROCEED with caution}, the section between the current station and
	the next station is occupied, or the next signal is set to a \doublered. \\
\autosignal{red} & \textbf{STOP}, the section between the current station and the next station
	is occupied. \\
\end{tabular}

\subsection{Signal Timers}
If an interlocking signal is set to \doublered, a timer can be set so that it clears
after a certain amount of time that a train arrives at the signal. The signal is then
automatically set to \doublegreen{} once the timer runs out. Timers are not limited to
multiples of 10 seconds.

The signal selector for a signal set to \doublered{} is shown on the left.
Clicking on the clock icon shows a menu like the one on the right, where
the timer can be set or cancelled.

\begin{tabular}{m{0.45\textwidth}m{0.45\textwidth}}
\begin{center}
\begin{tikzpicture}
	\fill[tcgray] (-1.2,-0.5) -- (1.7,-0.5) -- (1.7,0.8) -- (-1.2,0.8);
	\node[text=tclayout,anchor=east] at (-0.15,0.3) {\small\mono{UNI}};
	\fill[white]    (-0.4,0)  circle (0.3em);
	\draw[black]    (-0.32,0) -- (-0.4,0) -- (-0.4,0.08);
	\fill[sigred]   (0,0.3)   circle (0.3em);
	\fill[sigred]   (0,0)     circle (0.3em);
	\fill[siggreen] (0.4,0.3) circle (0.3em);
	\fill[siggreen] (0.4,0)   circle (0.3em);
	\fill[sigamber] (0.8,0.3) circle (0.3em);
	\fill[sigamber] (0.8,0)   circle (0.3em);
	\fill[sigamber] (1.2,0.3) circle (0.3em);
	\fill[siggreen] (1.2,0)   circle (0.3em);
	\draw[white,very thick] (-0.2,-0.2) -- (0.2,-0.2) -- (0.2,0.5) -- (-0.2,0.5) -- cycle;
\end{tikzpicture}
\end{center}
&
\begin{center}
\includegraphics[width=3cm]{clocksel.png}
\end{center}
\end{tabular}

Once the timer is set, the time is shown next to the selector:

\begin{center}
\begin{tikzpicture}
	\fill[tcgray] (-1.2,-0.5) -- (1.7,-0.5) -- (1.7,0.8) -- (-1.2,0.8);
	\node[text=tclayout,anchor=east] at (-0.15,0.3) {\small\mono{UNI}};
	\fill[white]    (-0.4,-0.03) circle (0.3em);
	\draw[black]    (-0.32,-0.03) -- (-0.4,-0.03) -- (-0.4,0.05);
	\node[text=white,anchor=east] at (-0.42,-0.03) {\scriptsize\mono{20}};
	\fill[sigred]   (0,0.3)   circle (0.3em);
	\fill[sigred]   (0,0)     circle (0.3em);
	\fill[siggreen] (0.4,0.3) circle (0.3em);
	\fill[siggreen] (0.4,0)   circle (0.3em);
	\fill[sigamber] (0.8,0.3) circle (0.3em);
	\fill[sigamber] (0.8,0)   circle (0.3em);
	\fill[sigamber] (1.2,0.3) circle (0.3em);
	\fill[siggreen] (1.2,0)   circle (0.3em);
	\draw[white,very thick] (-0.2,-0.2) -- (0.2,-0.2) -- (0.2,0.5) -- (-0.2,0.5) -- cycle;
\end{tikzpicture}
\end{center}

The timer can be cancelled by clicking the same clock icon and selecting \textit{Cancel Time},
or setting the signal to a favorable aspect.

\subsection{Storage Tracks}
Trains can be sent into storage tracks by selecting a \doubleamber{} signal at the station
immediately before it in the original direction (towards Downsview, Kipling or Don Mills)
if it is a centre track, or at the station it is attached to for a pocket track.
A button to select the storage track entrance appears on the operator's signal display.

The behaviour of storage track signals is as follows:

\textbf{Centre tracks}: The train exits in the direction it was originally traveling,
e.g. if a train was routed into the York Mills centre track from York Mills, it will
depart towards Lawrence; if it came from Lawrence, it will depart towards York Mills.

\textbf{Pocket tracks}: The train exits into the station in the direction it was sent
in from, e.g. if a train was routed into the Eglinton pocket track from the southbound
platform, it will be sent back to southbound.

\subsection{Headways}
The times displayed above and below each platform are the times in minutes and seconds
since a train last departed or arrived at the station.

\begin{center}
\includegraphics[height=1.4in]{headways.png}
\end{center}

Note that each time is a different color above; times are color coded depending on
the amount of time elapsed. The colors are as follows:

\begin{center}
\begin{tabular}{c@{$\qquad$}c@{$\qquad$}c@{$\qquad$}c}
\textbf{YUS} & {\color{siggreen} 0:00 -- 4:00} & {\color{sigamber} 4:01 -- 6:00} & {\color{sigred} 6:01 or more} \\
\textbf{BD} & {\color{siggreen} 0:00 -- 5:00} & {\color{sigamber} 5:01 -- 8:00} & {\color{sigred} 8:01 or more} \\
\end{tabular}
\end{center}

\subsection{Train Menu}
The train menu lets the controller move trains around the line, and reset trips and alarms.
When a train is clicked in the train list, the train menu opens:

\begin{center}
\includegraphics[width=2in]{trainmenu.png}
\end{center}

\subsubsection{Moving Trains}
Trains can be moved in the train menu by typing the code corresponding
to the desired location (this can be a station or a storage track), and
clicking the desired direction below (in this case, \mono{towards
Downsview} or \mono{towards Finch}).

See \hyperref[train-location]{Train Locations} and
\hyperref[station-codes]{Station Codes} for the complete list of location
codes. Station codes are also shown above each station on the layout.

\subsubsection{Resetting Trips and Alarms}
Trips and alarms can be reset by using the \mono{Reset Trip} and \mono{Reset Alarm}
functions respectively in the train menu.

\newpage
\section{Train Statuses}
\label{train-status}
Below is the list of all possible statuses for a train.
The following statuses are considered normal and no action needs to be taken:

\begin{tabular}{p{0.24\linewidth}p{0.7\linewidth}}
\status{blue}{arrived} & The train is arrived and stopped at a station or en-route signal. \\
\status{blue}{door open} & The train is stopped at a station and the doors are open. \\
\status{green}{departed} & The train has departed a station or en-route signal. \\
\end{tabular}

The following statuses are considered issues and do require action.
The train's indicator will also be colored accordingly and an exclamation mark icon
shown next to it.

\begin{tabular}{p{0.24\linewidth}p{0.7\linewidth}}
\status{orange}{door open} & The doors are open \textit{while not stopped at a station}.
		This includes having departed a station, stopped at an en-route signal,
		or stopped in a storage track. \\
\status{orange}{tripped} & The train's emergency brake is activated by tripping a signal. \\
\status{orange}{violated} & The train's emergency brake is activated by departing on
		an unfavorable signal. \\
\status{orange}{overshot} & The train overshot the platform and needs to reverse to
		position itself properly on the platform. \\
\status{orange}{overspeed} & The train was going faster than the safety system allows
		(25 km/h departing a station, 80 km/h line speed). \\
\status{orange}{plugin disabled} & The safety system is disabled in the simulator.
		Press the \mono{SECURITY\textunderscore D} key once (\mono{2} by default) to re-enable.\\
\status{yellow}{alarm} & The train has a yellow alarm. \\
\status{red}{alarm} & The train has a red alarm. \\
\end{tabular}

\newpage
\section{Train Locations}
\label{train-location}
Train locations are displayed in one of the following ways:

\begin{tabular}{p{0.2\linewidth}p{0.73\linewidth}}
\location{FIN} & The train is at a station (in this case, Finch) or a storage track. \\
\location{SCW.X6} & The train is stopped in front of the specified en-route signal. \\
\location{YKM.X14-LAW} & The train has departed the first location (\mono{YKM.X14}) and is en
		route to the second location (\mono{LAW}). \\
\end{tabular}

Each station in the system is assigned a three-letter station code; see
\hyperref[station-codes]{Station Codes} for the full listing. Storage tracks
are also given a four-letter code based on the nearest station:

\begin{tabular}{clcl}
\mono{CHEC} & Chester Centre Track			& \mono{OSSC} & Ossington Centre Track \\
\mono{DAVY} & Davisville 3rd Platform		& \mono{SCWC} & St. Clair West Centre Track \\
\mono{EGLP} & Eglinton Pocket Track			& \mono{UNIC} & Union Centre Track \\
\mono{ISLC} & Islington Centre Track		& \mono{WILY} & Wilson Hostler \\
\mono{LWWC} & Lawrence West Centre Track$\qquad$
	& \mono{YKMC} & York Mills Centre Track \\
\mono{OSGP} & Osgoode Pocket Track \\
\end{tabular}

En-route signals each have codes as well based on the previous station (for automatic
signals) or the closest station (for interlocking signals).

\begin{tabular}{lcl}
\mono{FIN.S591} & Southbound & Automatic signal \textbf{S 591} south of Finch \\
\mono{YKM.X60} & Southbound & Approach signal \textbf{X 60} north of York Mills \\
\mono{YKM.X14} & Southbound & Home signal \textbf{X 14} south of York Mills \\
\mono{EGL.X66} & Southbound & Home signal \textbf{X 66} north of Eglinton \\
\mono{SCW.X6} & Northbound & Home signal \textbf{X 6} north of St. Clair West \\
\mono{SCW.SP122} & Northbound & Automatic signal \textbf{SP 122} north of St. Clair West \\
\mono{WIL.X76} & Northbound & Home signal \textbf{X 76} north of Wilson \\
\end{tabular}

\begin{tabular}{lcl}
\mono{WAR.X34} & Westbound & Home signal \textbf{X 34} east of Warden \\
\mono{VPK.X18} & Westbound & Home signal \textbf{X 18} east of Victoria Park \\
\mono{ISL.X48} & Westbound & Home signal \textbf{X 48} east of Islington \\
\mono{KIP.X18} & Westbound & Home signal \textbf{X 18} east of Kipling \\
\end{tabular}

\newpage
\section{Station Codes}
\label{station-codes}
\begin{tabular}{clclcl}
\mono{BAT} & Bathurst			& \mono{KEN} & Kennedy				& \mono{UNI} & Union \\
\mono{BAY} & Bay				& \mono{KNG} & King					& \mono{VPK} & Victoria Park \\
\mono{BYV} & Bayview			& \mono{KIP} & Kipling				& \mono{WAR} & Warden \\
\mono{BSS} & Bessarion			& \mono{LAN} & Lansdowne			& \mono{WEL} & Wellesley \\
\mono{BLO} & Bloor-Yonge (YUS)$\quad$
								& \mono{LAW} & Lawrence				& \mono{WIL} & Wilson \\
\mono{YNG} & Bloor-Yonge (BD)	& \mono{LWW} & Lawrence West		& \mono{WDB} & Woodbine \\
\mono{BRD} & Broadview			& \mono{LES} & Leslie				& \mono{YKM} & York Mills \\
\mono{CFK} & Castle Frank		& \mono{MST} & Main Street			& \mono{YKD} & Yorkdale \\
\mono{CHE} & Chester			& \mono{MUS} & Museum				\\
\mono{CHR} & Christie			& \mono{NYC} & North York Centre	\\
\mono{COL} & College			& \mono{OML} & Old Mill				\\
\mono{COX} & Coxwell			& \mono{OSG} & Osgoode				\\
\mono{DAV} & Davisville			& \mono{OSS} & Ossington			\\
\mono{DML} & Don Mills			& \mono{PAP} & Pape					\\
\mono{DON} & Donlands			& \mono{QUN} & Queen				\\
\mono{DWN} & Downsview			& \mono{QPK} & Queen's Park			\\
\mono{DUF} & Dufferin			& \mono{ROS} & Rosedale				\\
\mono{DUN} & Dundas				& \mono{RYK} & Royal York			\\
\mono{DNW} & Dundas West		& \mono{RUN} & Runnymede			\\
\mono{DUP} & Dupont				& \mono{SHP} & Sheppard-Yonge (YUS)$\quad$	\\
\mono{EGL} & Eglinton			& \mono{SHY} & Sheppard-Yonge (SHP)	\\
\mono{EGW} & Eglinton West		& \mono{SHE} & Sherbourne			\\
\mono{FIN} & Finch				& \mono{SPA} & Spadina				\\
\mono{GCN} & Glencairn			& \mono{STA} & St. Andrew			\\
\mono{GRN} & Greenwood			& \mono{STC} & St. Clair			\\
\mono{HPK} & High Park			& \mono{SCW} & St. Clair West		\\
\mono{ISL} & Islington			& \mono{STG} & St. George			\\
\mono{JAN} & Jane				& \mono{STP} & St. Patrick			\\
\mono{KEL} & Keele				& \mono{SUM} & Summerhill			\\
\end{tabular}

\begin{titlepage}
\pagecolor{tcgray}
\null
\vspace{\stretch{1}}
\includegraphics{logo.eps}
\end{titlepage}

\end{document}
