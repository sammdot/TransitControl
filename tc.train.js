/*
 * Train module for the Transit Control display.
 * (c) Sammi de Guzman, 2016.
 */

Train = function (train) {
	this.update(train);
	this.screenPosition = null;

	return this;
};

Train.prototype.update = function (train) {
	this.run = train.run;
	this.location = train.location;
	this.lastTime = train.lastime;
	this.model = train.train;
	this.pluginEnabled = train.system == 1;
	this.schedule = train.onTime || "-";
	this.operator = train.operator || "-";
	this.badge = train.badge || "-";
	this.yard = train.yard || "-";
	if (train.doors) this.doors = train.doors;
	if (train.redAlarm) this.redAlarm = true;
	if (train.yellowAlarm) this.yellowAlarm = true;
	if (train.tripped) this.tripped = true;
	if (train.overshot) this.overshot = true;
	if (train.violated) this.violated = true;
	if (train.overspeed) this.overspeed = true;
};

Train.prototype.hasMinorIssue = function () {
	return this.tripped || this.overshot || this.violated || this.overspeed ||
		!this.pluginEnabled || (!this.isAtStation() &&
			this.doors && (this.doors.left || this.doors.right)) || false;
};

Train.prototype.trainStatusToClass = function () {
	if (this.redAlarm) {
		return "red-alarm";
	} else if (this.yellowAlarm) {
		return "yellow-alarm";
	} else if (this.hasMinorIssue()) {
		return "issue";
	} else {
		return "ok";
	}
};

Train.prototype.statusToClass = function () {
	if (this.redAlarm) {
		return "red-alarm";
	} else if (this.yellowAlarm) {
		return "yellow-alarm";
	} else if (this.hasMinorIssue()) {
		return "issue";
	} else if (this.location.departed) {
		return "in-transit";
	} else {
		return "ready";
	}
};

Train.prototype.displayStatus = function () {
	if (this.redAlarm || this.yellowAlarm) {
		return "alarm";
	} else if (!this.pluginEnabled) {
		return "plugin disabled";
	} else if (this.tripped) {
		return "tripped";
	} else if (this.overshot) {
		return "overshot";
	} else if (this.violated) {
		return "violated";
	} else if (this.overspeed) {
		return "overspeed";
	} else if (this.doors && (this.doors.left || this.doors.right)) {
		return "door open";
	} else if (this.location.departed) {
		return "departed";
	} else {
		return "arrived";
	}
};

Train.prototype.displayLastTime = function () {
	var timeDiff = Math.floor(new Date().getTime() / 1000) - this.lastTime;
	var hrs = 0,
		mins = Math.floor(timeDiff / 60),
		secs = timeDiff % 60;
	if (mins > 59) {
		hrs = Math.floor(mins / 60);
		mins = mins % 60;
		if (mins < 10) mins = "0" + mins;
	}
	if (secs < 10) secs = "0" + secs;

	if (hrs == 0) return mins + ":" + secs;
	else return hrs + ":" + mins + ":" + secs;
};

Train.prototype.isAtStation = function () {
	return this.location.departed == 0 && this.location.enroutesPast == 0 &&
		(("" + this.location.station).search(/a/) == -1);
};

Train.prototype.isAtEnroute = function () {
	return this.location.departed == 0 && this.location.enroutesPast != 0;
};
