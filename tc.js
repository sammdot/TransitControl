/*!
 * Transit Control display for the Virtual TTC Academy.
 * Not intended for use anywhere outside the Academy site.
 * (c) Sammi de Guzman, 2016.
 */

TransitControl = function (canvasDiv, trainListDiv, trainMenuDiv, clockMenuDiv, style) {
	this.canvas = {};
	this.canvas.element = document.getElementById(canvasDiv);
	this.canvas.context = this.canvas.element.getContext("2d");

	if (!this.canvas.context)
		return null;

	this.trainList = document.getElementById(trainListDiv);
	this.trains = {};
	this.strips = {};
	this.signals = {};

	this.activeStrip = null;
	this.trainMenu = document.getElementById(trainMenuDiv);
	[].slice.call(this.trainMenu.getElementsByClassName("menu-action"))
		.forEach(function (action) {
			var self = this;
			action.onclick = function () {
				if (!this.classList.contains("inactive") && !this.classList.contains("disabled"))
					self.trainMenuActionClicked(this.dataset.action, self.activeStrip.dataset.run);
			};
		}, this);

	this.activeSignal = null;
	this.clockMenu = document.getElementById(clockMenuDiv);
	[].slice.call(this.clockMenu.getElementsByClassName("menu-action"))
		.forEach(function (action) {
			var self = this;
			action.onclick = function () {
				if (!this.classList.contains("inactive") && !this.classList.contains("disabled"))
					self.clockMenuActionClicked(this.dataset.action, self.activeSignal);
			};
		}, this);

	this.style = {};
	Object.assign(this.style, style);

	var self = this;
	this.canvas.element.addEventListener("click", function (e) {
		self.canvasClicked(e);
	});

	return this;
};

TransitControl.prototype.stnCodeToNumber = function (stnCode) {
	var num = this.stations.indexOf(stnCode);
	if (num >= 0) {
		return num;
	} else if (this.otherCodes[stnCode]) {
		return this.otherCodes[stnCode];
	} else {
		return "-";
	}
};

TransitControl.prototype.update = function (data) {
	var updateLayout = this.line != data.line;

	this.line = data.line;

	if (updateLayout) {
		this.populateStations();
		this.layout = {};
		this.populateLayout();
	}

	this.occupiedStations = [[], []];
	this.occupiedEnroutes = [[], []];

	var runs = [];
	data.trains.forEach(function (train) { runs.push(train.run); });
	Object.keys(this.trains).forEach(function (run) {
		if (runs.indexOf(run) < 0) { delete this.trains[run]; }
	}, this);
	data.trains.forEach(function (train) {
		if (train.run == 0) return;
		if (this.trains[train.run]) {
			this.trains[train.run].update(train);
		} else {
			this.trains[train.run] = new Train(train);
		}

		if (this.trains[train.run].isAtStation()) {
			this.occupiedStations[train.location.direction].push(train.location.station);
		} else if (this.trains[train.run].isAtEnroute()) {
			this.occupiedEnroutes[train.location.direction].push(
				[train.location.station, train.location.enroutesPast]);
		}
	}, this);

	data.signals.forEach(function (signal) {
		var stn = signal.station;
		if (signal.type == 0) { // station
			if (this.signals[stn]) {
				this.signals[stn][0] && this.signals[stn][0].update(signal[0]);
				this.signals[stn][1] && this.signals[stn][1].update(signal[1]);
			} else {
				this.signals[stn] = [
					new Signal(stn, 0, signal[0]),
					this.line == 2 ? null : // TODO: BD
					new Signal(stn, 1, signal[1])
				];
			}
		} else if (signal.type == 1) { // enroute
			if (this.signals[stn]) {
				this.signals[stn][0] && this.signals[stn][0].update(signal[0]);
				this.signals[stn][1] && this.signals[stn][1].update(signal[1]);
			} else {
				this.signals[stn] = [
					signal[0] ? new Signal(stn, 0, signal[0]) : null,
					signal[1] ? new Signal(stn, 1, signal[1]) : null
				];
			}
		} else if (signal.type == 2) { // storage track
			var sig;
			if (this.signals[stn]) {
				sig = this.signals[stn][0];
				sig.update(signal[0]);
			} else {
				sig = new Signal(stn, 0, signal[0]);
			}
			this.signals[stn] = [sig, sig];
		}
	}, this);

	this.redraw();
};

TransitControl.prototype.redraw = function () {
	this.fixStrips();
	this.drawCanvas();
};

TransitControl.prototype.fixStrips = function () {
	Object.keys(this.strips).forEach(function (run) {
		if (!this.trains[run]) {
			if (this.activeStrip == this.strips[run]) {
				this.toggleStrip(this.activeStrip);
			}
			this.strips[run].remove();
			delete this.strips[run];
		}
	}, this);

	Object.keys(this.trains).forEach(function (run) {
		if (!this.strips[run]) {
			this.strips[run] = this.createStrip(this.trains[run]);
			this.trainList.appendChild(this.strips[run]);
		}
	}, this);

	var strips = this.trainList.children;
	var stripsArr = [];
	for (var i = 0; i < strips.length; ++i)
		stripsArr.push(strips[i]);

	stripsArr.sort(function (a, b) {
		return a.dataset.run == b.dataset.run ? 0 :
			(a.dataset.run > b.dataset.run ? 1 : -1);
	});
	stripsArr.forEach(function (strip) {
		this.updateStrip(strip);
		this.trainList.appendChild(strip);
	}, this);
};

TransitControl.prototype.createStrip = function (train) {
	function div() { return document.createElement("div"); }
	function span() { return document.createElement("span"); }

	var el = document.createElement("li");
	el.classList.add("train");
	el.id = "run-" + train.run;
	el.dataset.run = train.run;

	var topDiv = div();
	var runNumber = span();
	runNumber.classList.add("run-number");
	runNumber.textContent = train.run;
	var status_ = span();
	status_.classList.add("status");
	var statusText = span();
	statusText.classList.add("status-text");
	var lastTime = span();
	lastTime.classList.add("last-time");
	status_.appendChild(statusText);
	status_.appendChild(lastTime);
	var operator = span();
	operator.classList.add("operator");
	operator.textContent = train.operator;
	topDiv.appendChild(runNumber);
	topDiv.appendChild(status_);
	topDiv.appendChild(operator);
	el.appendChild(topDiv);

	var bottomDiv = div();
	var trainType = span();
	trainType.classList.add("train-type");
	trainType.textContent = train.model;
	var location_ = span();
	location_.classList.add("location");
	var schedule = span();
	schedule.classList.add("schedule");
	bottomDiv.appendChild(trainType);
	bottomDiv.appendChild(location_);
	bottomDiv.appendChild(schedule);
	el.appendChild(bottomDiv);

	var self = this;
	el.onclick = function (e) { self.toggleStrip(el); };

	return el;
};

TransitControl.prototype.updateTrainMenu = function () {
	var train = this.trains[this.activeStrip.dataset.run];
	if (train.redAlarm || train.yellowAlarm) {
		this.trainMenu.querySelector("[data-action='reset-alarm']").classList.remove("disabled");
	} else {
		this.trainMenu.querySelector("[data-action='reset-alarm']").classList.add("disabled");
	}

	if (train.tripped || train.overshot || train.violated || train.overspeed) {
		this.trainMenu.querySelector("[data-action='reset-trip']").classList.remove("disabled");
	} else {
		this.trainMenu.querySelector("[data-action='reset-trip']").classList.add("disabled");
	}

	if (this.line == 2) {
		this.trainMenu.querySelector("[data-action='move-reverse']").classList.add("disabled");
	}
};

TransitControl.prototype.toggleStrip = function (strip) {
	if (this.activeStrip == strip) {
		strip.classList.remove("active");
		this.activeStrip = null;
		this.trainMenu.querySelector("#station-code").value = "";
		this.trainMenu.classList.remove("open");
	} else {
		if (this.activeStrip) {
			this.activeStrip.classList.remove("active");
		}
		this.activeStrip = strip;
		strip.classList.add("active");
		this.trainMenu.style.top = strip.offsetTop + "px";
		this.trainMenu.querySelector("#station-code").value = "";
		this.trainMenu.querySelector("#station-code").classList.remove("error");
		this.updateTrainMenu();
		this.trainMenu.classList.add("open");
	}
};

TransitControl.prototype.displayLocation = function (loc) {
	if (!Number.isInteger(loc.station)) {
		var stn;
		for (var code in this.otherCodes) {
			if (this.otherCodes.hasOwnProperty(code)) {
				if (this.otherCodes[code] == loc.station) {
					stn = code;
				}
			}
		}
		return stn;
	} else if (loc.departed == 0 && loc.enroutesPast == 0) {
		return this.stations[loc.station];
	} else if (loc.departed == 0 && loc.enroutesPast) {
		return this.enroutes[loc.direction][loc.station][loc.enroutesPast];
	} else {
		var prev, next;

		if (loc.enroutesPast == 0) {
			prev = this.stations[loc.station];
		} else {
			prev = this.enroutes[loc.direction][loc.station][loc.enroutesPast];
		}

		nextEnroute = loc.enroutesPast + 1;
		if (this.enroutes[loc.direction][loc.station] &&
			this.enroutes[loc.direction][loc.station][nextEnroute]) {
			next = this.enroutes[loc.direction][loc.station][nextEnroute];
		} else {
			next = this.stations[loc.station + (loc.direction == 0 ? 1 : -1)];
		}

		return prev + "-" + next;
	}
};

TransitControl.prototype.updateStrip = function (strip) {
	var train = this.trains[strip.dataset.run];

	strip.classList.remove("ok", "issue", "red-alarm", "yellow-alarm");
	strip.classList.add(train.trainStatusToClass());

	function getElement (cls) {
		return strip.getElementsByClassName(cls)[0];
	}

	getElement("run-number").textContent = train.run;
	getElement("status").classList.remove("ready", "in-transit", "issue", "red-alarm", "yellow-alarm");
	getElement("status").classList.add(train.statusToClass());
	getElement("status-text").textContent = train.displayStatus();
	getElement("operator").textContent = train.operator;
	getElement("train-type").textContent = train.model;
	getElement("location").textContent = this.displayLocation(train.location);

	var sched = getElement("schedule");
	sched.classList.remove("on-time", "early", "late");
	sched.textContent = train.operator !== "-" ? ("" + train.badge + "(" + train.yard + ")") : "-";
};

TransitControl.prototype.updateTimes = function () {
	Object.keys(this.strips).forEach(function (run) {
		this.strips[run].getElementsByClassName("last-time")[0]
			.textContent = this.trains[run].displayLastTime();
	}, this);
};

TransitControl.prototype.trainMenuActionClicked = function (action, run) {
	if (action == "reset-trip") {
		this.resetTrip(run);
	} else if (action == "reset-alarm") {
		this.resetAlarm(run);
	} else if (action == "move-original" || action == "move-reverse") {
		var direction = (action == "move-original") ? 0 : 1;
		var stnCode = this.stnCodeToNumber(this.trainMenu.querySelector("#station-code")
			.value.toUpperCase());
		if (stnCode == "-") {
			this.trainMenu.querySelector("#station-code").classList.add("error");
			this.trainMenu.querySelector("#station-code").focus();
			return;
		} else {
			this.moveTrain(run, stnCode, direction);
		}
	}
	this.toggleStrip(this.activeStrip);
};

TransitControl.prototype.toggleClockMenu = function (signal) {
	var clock = signal.clockSelector;

	if (this.activeSignal == signal) {
		this.activeSignal = null;
		this.clockMenu.querySelector("#timer-seconds").value = "";
		this.clockMenu.classList.remove("open");
	} else {
		this.activeSignal = signal;
		this.clockMenu.style.top = (clock[1] + 2 * clock[2]) + "px";
		this.clockMenu.style.left = (clock[0] - clock[2]) + "px";
		this.clockMenu.querySelector("#timer-seconds").value = "" + (signal.time || "");
		this.updateClockMenu();
		this.clockMenu.classList.add("open");
		this.clockMenu.querySelector("#timer-seconds").focus();
	}
};

TransitControl.prototype.updateClockMenu = function () {
	this.clockMenu.querySelector("[data-action='set-time']").classList.remove("disabled");
	if (this.activeSignal.time) {
		this.clockMenu.querySelector("[data-action='cancel-time']").classList.remove("disabled");
	} else {
		this.clockMenu.querySelector("[data-action='cancel-time']").classList.add("disabled");
	}
};

TransitControl.prototype.clockMenuActionClicked = function (action, signal) {
	if (action == "set-time") {
		var time = parseInt(this.clockMenu.querySelector("#timer-seconds").value) || null;
		if (time == null) {
			this.clockMenu.querySelector("#timer-seconds").value = "";
			this.clockMenu.querySelector("#timer-seconds").focus();
			return;
		} else {
			this.setSignalTime(signal.station, signal.direction, time);
		}
	} else if (action == "cancel-time") {
		this.cancelSignalTime(signal.station, signal.direction);
	}
	this.toggleClockMenu(signal);
};

/*
CONTROL DATA INFO

for each train:
	location.station = stationID
	location.enroutesPast = floor(depart / 2)
	location.departed = depart % 2
	system = tripping system (1 = enable)
	doors.left = doors % 2 == 1
	doors.right = doors > 1
	operator.badge = openbvesignin
	operator.name = (JOIN)
	redAlarm = alarm == 1
	yellowAlarm = alarm == 2
	tripped = trip == 1
	overshot = trip == 2
	violated = trip == 3
	overspeed = trip == 4

for each station:
	id = station_id
	code = stationid
	type: 0 = station, 1 = enroute, 2 = storage
		stracktype: 1 = centre, 1 = pocket
	aspect: 0 = red, 1 = G/G, 2 = A/A, 3 = A/G
	aspectype: 0 = auto, 3 = no-crossover, 4 = all
	time = timestamp when timer clears
	direction = direction of enroute (enroutes only)

direction: 0 = original (DOWN), 1 = reverse (UP)
*/
